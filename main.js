// fetch("https://dog.ceo/api/breeds/list/all").then(function (response) {
//     return response.json();
// }).then(function (data) {
//     console.log(data);
// })
let timer;
let deleteFirstPhotoDelay;

async function start() {
    // const response = await fetch("https://dog.ceo/api/breeds/list/all"); // await voert volgende codeblocks uit en keert dan terug
    // const data = await response.json();
    // // console.log(data);
    // createBreedList(data.message);
    try {
        const response = await fetch("https://dog.ceo/api/breeds/list/all"); // await voert volgende codeblocks uit en keert dan terug
        const data = await response.json();
        createBreedList(data.message);
    } catch (error) {
        alert("There was a problem fetching the breed list");
    }
}
start();

// function createBreedList(breedList) { // breedList can be anything (x, ...)
//     document.getElementById("breed").innerHTML = `
//     <select>
//     <option>Choose a dog breed</option>
//     <option>Corgi</option>
//     <option>Boxer</option>
//     <option>Bulldog</option>
//     </select>
//     `
// }
function createBreedList(breedList) { // breedList can be anything (x, ...)
    document.getElementById("breed").innerHTML = `
    <select onchange="loadByBreed(this.value)">
        <option>Choose a dog breed</option>
        ${Object.keys(breedList) // create array of breed
            .map(function (breed) {
                return `<option>${breed}</option>`
            })
            .join('')}
    </select>
    `
}
async function loadByBreed(breed) {
    // alert(breed);
    if (breed != "Choose a dog breed") {
        const response = await fetch(`https://dog.ceo/api/breed/${breed}/images`); // between back tick = dynamic; breed replaces url standard value (hound)
        const data = await response.json();
        // console.log(data);
        createSlideshow(data.message);
    }
}
function createSlideshow(images) {
    // console.log(images);
    let currentPosition = 0;
    clearInterval(timer);
    clearTimeout(deleteFirstPhotoDelay);

    // document.getElementById("slideshow").innerHTML = `
    // <div class="slide" style="background-image: url('${images[0]}')"></div>
    // <div class="slide" style="background-image: url('${images[1]}')"></div>
    // `
    // currentPosition += 2;
    // timer = setInterval(nextSlide, 3000);
    if (images.length > 1) {
        document.getElementById("slideshow").innerHTML = `
        <div class="slide" style="background-image: url('${images[0]}')"></div>
        <div class="slide" style="background-image: url('${images[1]}')"></div>
        `
        currentPosition += 2;
        if (images.length == 2) { currentPosition = 0; }
        timer = setInterval(nextSlide, 3000);
    } else {
        document.getElementById("slideshow").innerHTML = `
        <div class="slide" style="background-image: url('${images[0]}')"></div>
        <div class="slide" style="background-image: url('${images[1]}')"></div>
        `
    }

    function nextSlide() {
        document.getElementById("slideshow").insertAdjacentHTML("beforeend", `<div class="slide" style="background-image: url('${images[currentPosition]}')"></div>`)
        deleteFirstPhotoDelay = setTimeout(function () {
            document.querySelector(".slide").remove()
        }, 1000);
        if (currentPosition + 1 >= images.length) {
            currentPosition = 0;
        } else {
            currentPosition++;
        }
    }
}